extends Area2D

# class member variables go here, for example:
# var a = 2
#export (int) var speed

var besarLayar
var speed = 300

onready var bullet = preload ("res://Scenes/Laser.tscn")


func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	besarLayar = get_viewport_rect().size

func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
	var velocityX = 0
	var velocityY = 0
	
	if Input.is_action_pressed("ui_right"):
		velocityX = 1
	if Input.is_action_pressed("ui_left"):
		velocityX = -1
	if Input.is_action_pressed("ui_up"):
		velocityY = -1
	if Input.is_action_pressed("ui_down"):
		velocityY = 1
		
	velocityX = velocityX * speed * delta
	velocityY = velocityY * speed * delta
	
	position.x += velocityX
	position.y += velocityY
	
	position.x = clamp(position.x, 0, besarLayar.x)
	position.y = clamp(position.y, 0, besarLayar.y)
	
	if Input.is_action_just_pressed("ui_select"):
		var n_bullet = bullet.instance()
		n_bullet.position = position
		n_bullet.position.y -= 30
		get_node("/root/Main").add_child(n_bullet)
